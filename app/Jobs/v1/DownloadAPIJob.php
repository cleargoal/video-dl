<?php

namespace App\Jobs\v1;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DownloadAPIJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var string
     */
    public string $videoUri;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($videoUri)
    {
        $this->videoUri = $videoUri;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        logger(PHP_EOL . Carbon::now() . ' -- Just running ',  ['video_uri' => $this->videoUri]);
        sleep(1);
        logger('Still working...');
        sleep(1);
        logger('Try to finish successfully...');
    }
}
