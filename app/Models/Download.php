<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Download extends Model
{
    use HasFactory;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'uri', 'job_uuid', 'start_download', 'finish_download', 'result',];

    protected $casts = [
        'user_id' => 'integer',
        'uri' => 'string',
        'job_uuid' => 'string',
        'start_download' => 'datetime',
        'finish_download' => 'datetime',
        'result' => 'string',
    ];

}
