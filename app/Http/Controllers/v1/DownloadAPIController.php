<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\v1\DownloadAPIRequest;
use App\Services\v1\DownloadAPIService;
use Illuminate\Http\JsonResponse;

class DownloadAPIController extends Controller
{

    /**
     * Download video API controller
     *
     * @param DownloadAPIRequest $request
     * @param DownloadAPIService $apiService
     * @return JsonResponse
     */
    public function singleDownload(DownloadAPIRequest $request, DownloadAPIService $apiService): JsonResponse
    {
        return response()
            ->json($apiService->dispatchDownloads($request->input('video_uri')));
    }
}
