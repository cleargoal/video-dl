<?php

namespace App\Listeners\v1;

use App\Services\v1\DownloadAPIService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class DownloadAPIListener
{
    /**
     * @var DownloadAPIService
     */
    private DownloadAPIService $service;


    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(DownloadAPIService $service)
    {
        $this->service = $service;
    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle(object $event)
    {
        $payload = $event->job->payload();
        $jobUuid = $payload['uuid'];
        $videoUri = unserialize($payload['data']['command'])->videoUri;

        $eventClass = get_class($event);
        logger( PHP_EOL . 'listener, $eventClass', [$eventClass] );
        switch ($eventClass) {
            case 'Illuminate\Queue\Events\JobProcessing':
                $result = '';
                $this->service->saveDownStart($jobUuid, $videoUri);
                break;
            case 'Illuminate\Queue\Events\JobProcessed':
                $result = 'Success';
                $this->service->saveDownFinish($jobUuid, $result);
                break;
            case 'Illuminate\Queue\Events\JobFailed':
                $result = 'Fail';
                $this->service->saveDownFinish($jobUuid, $result);
                break;
            default:
                $result = '';
        }

    }

}
