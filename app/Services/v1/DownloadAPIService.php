<?php

namespace App\Services\v1;

use App\Models\Download;
use Carbon\Carbon;
use App\Jobs\v1\DownloadAPIJob;
use phpDocumentor\Reflection\Types\Void_;

class DownloadAPIService
{

    public function dispatchDownloads(string $videoUri): string
    {
        DownloadAPIJob::dispatch($videoUri);
        return 'Processing...';
    }

    /**
     * Save Download to DB
     *
     * @param string $videoUri
     * @param string $jobUuid
     * @return void
     */
    public function saveDownStart(string $jobUuid, string $videoUri)
    {
        logger('saveDownStart, data:',  ['uri' => $videoUri, 'uuid' => $jobUuid]);
        $newDn = new Download();
        $newDn->user_id = 1;
        $newDn->uri = $videoUri;
        $newDn->job_uuid = $jobUuid;
        $newDn->start_download = Carbon::now();
        logger('data save', [$newDn->user_id, $newDn->uri, $newDn->job_uuid, $newDn->start_download,]);
        $newDn->save();
        logger('new Dn ID', ['id' => $newDn->id]);
    }

    /**
     * Save to DB tge result of job
     *
     * @param $jobUuid
     * @param $jobResult
     * @return void
     */
    public function saveDownFinish ($jobUuid, $jobResult)
    {
        logger('saveDownFinish');
        $found = Download::where('job_uuid', $jobUuid)->first();
        if ($found) {
            $found->result = $jobResult;
            $found->finish_download = Carbon::now();
            $found->save();
        }
    }

}
